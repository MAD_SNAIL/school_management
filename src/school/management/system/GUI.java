package school.management.system;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class GUI {

    public static void main(String[] args) {

        java.util.List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(new Teacher(1, "Mandisa Kent", 500));
        teacherList.add(new Teacher(2, "Mellisa Mccarthy", 750));
        teacherList.add(new Teacher(3, "John Lennon", 800));
        teacherList.add(new Teacher(4, "Karl Marx", 1200));
        teacherList.add(new Teacher(5, "Matt Haig", 1500));

        Student lily = new Student(1, "Lily James", 4);
        Student josh = new Student(2, "Josh Brolin", 9);
        Student andy = new Student(3, "Andy Schulz", 6);
        Student matthew = new Student(4, "Matthew Cox", 10);

        lily.payFees(5000);
        josh.payFees(6000);
        andy.payFees(9000);
        matthew.payFees(3000);

        List<Student> studentList = new ArrayList<>();
        studentList.add(lily);
        studentList.add(josh);
        studentList.add(andy);
        studentList.add(matthew);

        School ghs = new School(teacherList, studentList);

        JFrame frame = new JFrame();
        JButton button1 = new JButton("Teachers List");
        JButton button2 = new JButton("Students List");

        frame.getContentPane().add(button1);
        frame.getContentPane().add(button2);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        button1.setBounds(125, 100, 120, 50);
        button2.setBounds(125, 200, 120, 50);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JLabel label = new JLabel("Teachers");

                String html = "<html><body>";
                html += "<ul>";

                for (int i = 0; i < teacherList.size(); i++) {
                    Teacher teacher = teacherList.get(i);

                    if (teacher.getSalary() <= 500) {
                        html += "<li>" + teacher.getName() + " (Salary: <span style='color: red;'>" + teacher.getSalary() + " $</span>)</li>";
                    } else if (teacher.getSalary() > 1000) {
                        html += "<li>" + teacher.getName() + " (Salary: <span style='color:green;'>" + teacher.getSalary() + " $</span>)</li>";
                    } else if (teacher.getSalary() == 750) {
                        html += "<li>" + teacher.getName() + " (Salary: <span style='color:orange;'>" + teacher.getSalary() + " $</span>)</li>";
                    } else if (teacher.getSalary() > 750) {
                        html += "<li>" + teacher.getName() + " (Salary: <span style='color:blue;'>" + teacher.getSalary() + " $</span>)</li>";
                    }
                }

                html += "</ul>";

                html += "<ul>" +
                        "<li style='color:red;'>Minimum</li>" +
                        "<li style='color:orange;'>Medium</li>" +
                        "<li style='color:blue;'>Good</li>" +
                        "<li style='color:green;'>Very Good</li><ul>";

                html += "</body></html>";

                label.setText(html);
                label.setVerticalAlignment(JLabel.CENTER);
                label.setBounds(0, 0, 300, 300);

                JFrame frame2 = new JFrame();

                frame2.setSize(300, 300);
                frame2.setLayout(null);
                frame2.setVisible(true);
                frame2.setLocationRelativeTo(null);
                frame2.add(label, BorderLayout.CENTER);
                frame2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JLabel label = new JLabel("Students");

                String html = "<html><body>";
                html += "<ul>";

                for (int i = 0; i < studentList.size(); i++) {
                    Student student = studentList.get(i);

                    if (student.getGrade() <= 4) {
                        html += "<li>" + student.getName() + " (Grade: <span style='color: red;'>" + student.getGrade() + " </span>"
                                + "," + " Fees paid: " + student.getFeesPaid() + " $)</li>";
                    } else if (student.getGrade() == 5) {
                        html += "<li>" + student.getName() + " (Grade: <span style='color:orange;'>" + student.getGrade() + "</span>"
                                + "," + " Fees paid: " + student.getFeesPaid() + " $)</li>";
                    } else if (student.getGrade() < 8) {
                        html += "<li>" + student.getName() + " (Grade: <span style='color:blue;'>" + student.getGrade() + "</span>"
                                + "," + " Fees paid: " + student.getFeesPaid() + " $)</li>";
                    } else if (student.getGrade() >= 9) {
                        html += "<li>" + student.getName() + " (Grade: <span style='color:green;'>" + student.getGrade() + "</span>"
                                + "," + " Fees paid: " + student.getFeesPaid() + " $)</li>";
                    }
                }

                html += "</ul>";

                html += "<ul>" +
                        "<li style='color:red;'>Minimum</li>" +
                        "<li style='color:orange;'>Medium</li>" +
                        "<li style='color:blue;'>Good</li>" +
                        "<li style='color:green;'>Very Good</li><ul>";

                html += "</body></html>";

                label.setText(html);
                label.setVerticalAlignment(JLabel.CENTER);
                label.setBounds(0, 0, 300, 300);

                JFrame frame3 = new JFrame();

                frame3.setSize(320, 320);
                frame3.setLayout(null);
                frame3.setVisible(true);
                frame3.setLocationRelativeTo(null);
                frame3.add(label, BorderLayout.CENTER);
                frame3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });
    }
}
