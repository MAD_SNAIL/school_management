package school.management.system;

import java.util.List;

public class School {

    private List<Teacher> teachers;
    private List<Student> students;
    private static int totalMoneyEarned;
    private static int totalMoneySpent;

    /**
     *
     * @param teachers
     * @param students
     */
    public School(List<Teacher> teachers, List<Student> students) {
        this.teachers    = teachers;
        this.students    = students;
        totalMoneyEarned = 0;
        totalMoneySpent  = 0;
    }

    /**
     * Return the list of teachers in the school
     * @return
     */
    public List<Teacher> getTeachers() {
        return teachers;
    }

    /**
     *  Adds a teacher to the school
     * @param
     */
    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }

    /**
     * Return the list of students in the school
     * @return
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * Add a student to the school
     * @param student
     */
    public void addStudent(Student student) {
        students.add(student);
    }

    /**
     * Return the total money earned by the school
     * @return
     */
    public int getTotalMoneyEarned() {
        return totalMoneyEarned;
    }

    /**
     * Adds the total money earned by the school
     * @param moneyEarned
     */
    public static void updateTotalMoneyEarned(int moneyEarned) {
        totalMoneyEarned += moneyEarned;
    }

    /**
     * Return the total money spent by the school
     * @return
     */
    public int getTotalMoneySpent() {
        return totalMoneySpent;
    }

    /**
     * Update the money that is spent by the school
     * which is the salary given by the school to it's teachers.
     * @param moneySpent
     */
    public static void updateTotalMoneySpent(int moneySpent) {
        totalMoneyEarned -= moneySpent;
    }
}
